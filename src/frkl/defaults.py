# -*- coding: utf-8 -*-
from jinja2 import Environment

FRKL_CONTEXT_DEFAULT_KEY = "frkl_vars"
ENVIRONMENT_VARS_DEFAULT_KEY = "env"

DEFAULT_LEAF_DEFAULT_KEY = "default"

STEM_KEY_NAME = "child_marker"
DEFAULT_LEAF_KEY_NAME = "default_leaf"
DEFAULT_LEAF_DEFAULT_KEY_NAME = "default_leaf_key"
OTHER_VALID_KEYS_NAME = "other_keys"
DEFAULT_LEAF_KEY_MAP_NAME = "key_move_map"

# duplication of above vars, above vars will be removed after refactoring
CHILD_MARKER_NAME = "child_marker"
DEFAULT_LEAF_NAME = "default_leaf"
DEFAULT_LEAFKEY_NAME = "default_leaf_key"
OTHER_KEYS_NAME = "other_keys"
KEY_MOVE_MAP_NAME = "key_move_map"

START_VALUES_NAME = "init_values"

ALL_FORMAT = "*"
NONE_FORMAT = "-"
PYTHON_FORMAT = "PYTHON"
STRING_FORMAT = "STRING"
DICT_FORMAT = "DICT"

FRKL_DEFAULT_PARAMS = {
    STEM_KEY_NAME: "childs",
    DEFAULT_LEAF_KEY_NAME: "task",
    DEFAULT_LEAF_DEFAULT_KEY_NAME: "task_name",
    OTHER_VALID_KEYS_NAME: ["vars"],
    DEFAULT_LEAF_KEY_MAP_NAME: "vars",
}

OPT_PLACEHOLDER = -9877
NO_STEM_INDICATOR = "-99999"
RECURSIVE_LOAD_INDICATOR = "-67323"
DEFAULT_FRKL_JINJA_ENVIRONMENT = Environment()
