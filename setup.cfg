# This file is used to configure your project.
# Read more about the various options under:
# http://setuptools.readthedocs.io/en/latest/setuptools.html#configuring-setup-using-setup-cfg-files

[metadata]
name = frkl
description = Elastic dictionaries.
author = Markus Binsteiner
author-email = markus@frkl.io
license = "The Parity Public License 6.0.0"
url = https://gitlab.com/frkl/frkl
long-description = file: README.md
long-description-content-type = text/markdown
# Change if running only on Windows, Mac or Linux (comma-separated)
platforms = any
# Add here all kinds of additional classifiers as defined under
# https://pypi.python.org/pypi?%3Aaction=list_classifiers
classifiers =
    Development Status :: 4 - Beta
    Programming Language :: Python

[options]
zip_safe = False
packages = find:
include_package_data = True
package_dir =
    =src

setup_requires =
  setuptools_scm
  setuptools_scm_git_archive

install_requires =
    frutils>=1.0.0

python_requires = >=2.7,!=3.0.*,!=3.1.*,!=3.2.*,!=3.3.*,!=3.4.*

[options.packages.find]
where = src
exclude =
    tests


[options.extras_require]
# Add here additional requirements for extra features, to install with:
# `pip install frkl[PDF]` like:
# PDF = ReportLab; RXP
# Add here test requirements (semicolon/line-separated)
testing =
    pytest
    more-itertools==5.0.0    # support for Python 2.7
    pytest-cov
    tox

develop =
   flake8
   ipython
   black
   pip-tools
   pre-commit
   watchdog
   wheel
   pipdeptree
   isort
   mu-repo

docs =
   Sphinx
   recommonmark

[test]
# py.test options when running `python setup.py test`
# addopts = --verbose
extras = True

[tool:pytest]
# Options for py.test:
# Specify command line options as you would do when invoking py.test directly.
# e.g. --cov-report html (or xml) for html/xml output or --junitxml junit.xml
# in order to write a coverage file that can be read by Jenkins.
addopts =
    --cov frkl --cov-report term-missing
    --verbose
norecursedirs =
    dist
    build
    .tox
testpaths = tests
pep8maxlinelength = 88


[aliases]
build = bdist_wheel
release = build upload

[bdist_wheel]
# Use this option if your package is pure-python
universal = 1

[build_sphinx]
source_dir = docs
build_dir = docs/_build

[devpi:upload]
# Options for the devpi: PyPI server and packaging tool
# VCS export must be deactivated since we are using setuptools-scm
no-vcs = 1
formats = sdist, bdist_wheel

[flake8]
# Some sane defaults for the code style checker flake8
exclude =
    .tox
    build
    dist
    .eggs
    docs/conf.py
    .git
    __pycache__
ignore = F405, W503, E501
max-line-length = 88

[options.entry_points]
frkl.frk =
    expand_url = frkl.processors:UrlAbbrevProcessor
    read = frkl.processors:EnsureUrlProcessor
    deserialize = frkl.processors:EnsurePythonObjectProcessor
    frklize = frkl.processors:FrklProcessor
    render_template = frkl.processors:Jinja2TemplateProcessor
    regex = frkl.processors:RegexProcessor
    load_additional_configs = frkl.processors:LoadMoreConfigsProcessor
    to_yaml = frkl.processors:ToYamlProcessor
    merge = frkl.processors:MergeProcessor
    id = frkl.processors:IdProcessor
    inject = frkl.processors:DictInjectionProcessor
    split = frkl.processors:YamlTextSplitProcessor


frkl.collector =
    merge = frkl.callbacks:MergeResultCallback

frkl.frklists =
    default = frkl.frklist:DefaultFrklist
